/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/hashicorp/go-hclog"
	"github.com/jmoiron/sqlx"
	"github.com/sanity-io/litter"
	"github.com/stretchr/testify/assert"

	"gitlab.com/unboundsoftware/sloth/model"
)

func TestPostgres_Migration(t *testing.T) {
	tests := []struct {
		name    string
		setups  func(mock sqlmock.Sqlmock)
		wantErr bool
	}{
		{
			name: "success",
			setups: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT version_id, is_applied from goose_db_version ORDER BY id DESC").
					WillReturnError(fmt.Errorf("error"))
				mock.ExpectBegin()
				mock.ExpectExec("CREATE TABLE goose_db_version ( id integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, version_id bigint NOT NULL, is_applied boolean NOT NULL, tstamp timestamp NOT NULL DEFAULT now() )").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectExec("INSERT INTO goose_db_version (version_id, is_applied) VALUES ($1, $2)").
					WithArgs(0, true).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
				mock.ExpectQuery("SELECT version_id, is_applied from goose_db_version ORDER BY id DESC").
					WillReturnRows(sqlmock.NewRows([]string{"version_id", "is_applied"}).AddRow(0, true))

				mock.ExpectBegin()
				mock.ExpectExec("CREATE TABLE delayed ( id SERIAL PRIMARY KEY, until timestamptz not null, target text not null, payload bytea not null );").
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectExec("INSERT INTO goose_db_version (version_id, is_applied) VALUES ($1, $2)").
					WithArgs(20220114105200, true).
					WillReturnResult(sqlmock.NewResult(0, 1))
				mock.ExpectCommit()
			},
		},
		{
			name: "failure",
			setups: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT version_id, is_applied from goose_db_version ORDER BY id DESC").
					WillReturnError(fmt.Errorf("error"))
				mock.ExpectBegin()
				mock.ExpectExec("CREATE TABLE goose_db_version ( id integer PRIMARY KEY GENERATED BY DEFAULT AS IDENTITY, version_id bigint NOT NULL, is_applied boolean NOT NULL, tstamp timestamp NOT NULL DEFAULT now() )").
					WillReturnError(fmt.Errorf("error"))

			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockDb, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			db := sqlx.NewDb(mockDb, "mock")
			tt.setups(mock)

			err := runMigrations(db, "mock")
			if tt.wantErr {
				if err == nil {
					t.Errorf("expected error")
				}
			} else {
				if err != nil {
					t.Errorf("%v", err)
				}
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("%v", err)
			}
		})
	}
}
func TestPostgres_Start(t *testing.T) {
	type args struct {
		handler func(model.Request) error
	}
	tests := []struct {
		name        string
		setups      func(mock sqlmock.Sqlmock)
		quit        bool
		args        args
		wantErr     bool
		wantChanErr bool
		wantLogged  []string
	}{
		{
			name: "quit",
			setups: func(mock sqlmock.Sqlmock) {

			},

			quit:    true,
			args:    args{},
			wantErr: false,
		},
		{
			name: "error fetching requests",
			setups: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT id, target, payload FROM delayed WHERE until <= $1").
					WithArgs(time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)).
					WillReturnError(fmt.Errorf("err"))
			},

			args:        args{},
			wantErr:     false,
			wantChanErr: true,
			wantLogged: []string{
				"[ERROR] error fetching requests: error=err\n",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			quit := make(chan bool, 2)
			done := make(chan bool, 2)
			errChan := make(chan error, 2)
			var chanErr error
			if tt.quit {
				quit <- true
				done <- true
			} else {
				go func() {
					chanErr = <-errChan
					quit <- true
					done <- true
				}()
			}
			mockDb, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			db := sqlx.NewDb(mockDb, "mock")
			tt.setups(mock)

			m := &Postgres{
				client: db,
				timeSource: func() time.Time {
					return time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)
				},
				quit:     quit,
				loopDone: make(chan bool, 1),
				logger:   logger,
			}
			if err := m.Start(model.HandlerFunc(tt.args.handler), model.ChannelQuitter(errChan)); (err != nil) != tt.wantErr {
				t.Errorf("Start() error = %v, wantErr %v", err, tt.wantErr)
			}
			<-m.loopDone
			<-done
			close(errChan)
			if (chanErr != nil) != tt.wantChanErr {
				t.Errorf("Start() error = %v, wantChanErr %v", chanErr, tt.wantChanErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("%v", err)
			}
		})
	}
}

func TestPostgres_process(t *testing.T) {
	id := 123
	timeSource := func() time.Time {
		return time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)
	}
	type args struct {
		handler func(request model.Request) error
	}
	tests := []struct {
		name       string
		setups     func(mock sqlmock.Sqlmock)
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "no requests",
			setups: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT id, target, payload FROM delayed WHERE until <= $1").
					WithArgs(time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)).
					WillReturnRows(sqlmock.NewRows(nil))
			},
			args:       args{},
			wantErr:    false,
			wantLogged: nil,
		},
		{
			name: "error scanning data",
			setups: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT id, target, payload FROM delayed WHERE until <= $1").
					WithArgs(time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)).
					WillReturnRows(sqlmock.NewRows([]string{"id", "target", "payload"}).
						AddRow("abc", "", []byte("payload")))
			},

			args:       args{},
			wantErr:    true,
			wantLogged: []string{"[ERROR] error scanning data: error=\"sql: Scan error on column index 0, name \\\"id\\\": converting driver.Value type string (\\\"abc\\\") to a int: invalid syntax\"\n"},
		},
		{
			name: "error handling request",
			setups: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT id, target, payload FROM delayed WHERE until <= $1").
					WithArgs(time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)).
					WillReturnRows(sqlmock.NewRows([]string{"id", "target", "payload"}).
						AddRow(id, "test://host/path", []byte(`"abc"`)))
			},
			args: args{
				handler: func(request model.Request) error {
					want := model.Request{
						DelayedUntil: timeSource(),
						Target:       "test://host/path",
						Payload:      json.RawMessage(`"abc"`),
					}
					if !reflect.DeepEqual(request, want) {
						t.Errorf("process() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
					}
					return errors.New("error")
				},
			},
			wantErr:    true,
			wantLogged: []string{"[ERROR] error handling request: error=error\n"},
		},
		{
			name: "error acknowledging request",
			setups: func(mock sqlmock.Sqlmock) {
				mock.ExpectQuery("SELECT id, target, payload FROM delayed WHERE until <= $1").
					WithArgs(time.Date(2021, 2, 26, 14, 16, 23, 0, time.UTC)).
					WillReturnRows(sqlmock.NewRows([]string{"id", "target", "payload"}).
						AddRow(id, "test://host/path", []byte(`"abc"`)),
					)

				mock.ExpectExec("DELETE FROM delayed WHERE id=$1").
					WithArgs(id).
					WillReturnError(fmt.Errorf("error"))

			},
			args: args{
				handler: func(request model.Request) error {
					return nil
				},
			},
			wantErr:    false,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			litter.Config.HidePrivateFields = false
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			quit := make(chan bool, 2)
			mockDb, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			db := sqlx.NewDb(mockDb, "mock")
			tt.setups(mock)

			m := &Postgres{
				client:     db,
				timeSource: timeSource,
				sleep:      func(duration time.Duration) {},
				quit:       quit,
				loopDone:   make(chan bool, 1),
				logger:     logger,
			}
			if err := m.process(tt.args.handler); (err != nil) != tt.wantErr {
				t.Errorf("process() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("%v", err)
			}
		})
	}
}

func TestPostgres_Stop(t *testing.T) {
	tests := []struct {
		name       string
		wantErr    bool
		wantQuit   bool
		wantLogged []string
	}{
		{
			name:       "stopping",
			wantErr:    false,
			wantQuit:   true,
			wantLogged: []string{"[INFO]  stopping\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})

			quit := make(chan bool, 2)
			m := &Postgres{
				quit:    quit,
				logger:  logger,
				started: true,
			}
			if err := m.Stop(); (err != nil) != tt.wantErr {
				t.Errorf("Stop() error = %v, wantErr %v", err, tt.wantErr)
			}
			close(quit)
			q := <-quit
			if q != tt.wantQuit {
				t.Errorf("Stop() quit = %v, wantQuit %v", q, tt.wantQuit)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestPostgres_Add(t *testing.T) {
	type args struct {
		request model.Request
	}
	tests := []struct {
		name    string
		setups  func(mock sqlmock.Sqlmock)
		args    args
		wantErr bool
	}{
		{
			name: "error inserting",
			setups: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO delayed (until, target, payload) VALUES ($1,$2,$3)").
					WithArgs(time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC), "test://host/path", "payload").
					WillReturnError(fmt.Errorf("error"))
			},
			args: args{
				request: model.Request{
					DelayedUntil: time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
					Target:       "test://host/path",
					Payload:      []byte("payload"),
				},
			},
			wantErr: true,
		},
		{
			name: "success",
			setups: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("INSERT INTO delayed (until, target, payload) VALUES ($1,$2,$3)").
					WithArgs(time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC), "test://host/path", "payload").
					WillReturnResult(sqlmock.NewResult(0, 1))

			},
			args: args{
				request: model.Request{
					DelayedUntil: time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
					Target:       "test://host/path",
					Payload:      []byte("payload"),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockDb, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			db := sqlx.NewDb(mockDb, "mock")
			tt.setups(mock)

			m := &Postgres{
				client: db,
			}
			if err := m.Add(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("%v", err)
			}
		})
	}
}

func TestPostgres_remove(t *testing.T) {
	id := 123
	type fields struct {
		timeSource func() time.Time
		quit       chan bool
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		setups  func(mock sqlmock.Sqlmock)
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "error removing",
			setups: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("DELETE FROM delayed WHERE id=$1").
					WithArgs(id).
					WillReturnError(fmt.Errorf("error"))
			},
			args:    args{id: id},
			wantErr: true,
		},
		{
			name: "success",
			setups: func(mock sqlmock.Sqlmock) {
				mock.
					ExpectExec("DELETE FROM delayed WHERE id=$1").
					WithArgs(id).
					WillReturnResult(sqlmock.NewResult(0, 1))
			},
			args:    args{id: id},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockDb, mock, _ := sqlmock.New(sqlmock.QueryMatcherOption(sqlmock.QueryMatcherEqual))
			db := sqlx.NewDb(mockDb, "mock")
			tt.setups(mock)

			m := &Postgres{
				client:     db,
				timeSource: tt.fields.timeSource,
				quit:       tt.fields.quit,
			}
			if err := m.remove(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("remove() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err := mock.ExpectationsWereMet(); err != nil {
				t.Errorf("%v", err)
			}
		})
	}
}

func Test_storedRequest_Request(t *testing.T) {
	type fields struct {
		Until   time.Time
		Target  string
		Payload json.RawMessage
	}
	tests := []struct {
		name   string
		fields fields
		want   model.Request
	}{
		{
			name: "success",
			fields: fields{
				Until:   time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
				Target:  "test://host/path",
				Payload: json.RawMessage("abc"),
			},
			want: model.Request{
				DelayedUntil: time.Date(2021, 2, 26, 13, 53, 15, 0, time.UTC),
				Target:       "test://host/path",
				Payload:      json.RawMessage("abc"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := storedRequest{
				Until:   tt.fields.Until,
				Payload: tt.fields.Payload,
				Target:  tt.fields.Target,
			}
			if got := s.Request(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Request() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPostgres_Configure(t *testing.T) {
	tests := []struct {
		name       string
		args       []string
		connect    func(t *testing.T) func(string, string) (*sqlx.DB, error)
		want       bool
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "invalid parameter",
			args: []string{"--invalid"},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "unknown flag --invalid")
			},
			wantLogged: nil,
		},
		{
			name:       "no parameters",
			args:       []string{},
			want:       false,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name: "connection failure",
			args: []string{"--postgres-url", "postgres://a:b@:5432/db"},
			connect: func(t *testing.T) func(url, driver string) (*sqlx.DB, error) {
				return func(url, driver string) (*sqlx.DB, error) {
					assert.Equal(t, "postgres://a:b@:5432/db", url)
					return nil, fmt.Errorf("connection error")
				}
			},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "connection error")
			},
			wantLogged: nil,
		},
		{
			name: "success",
			args: []string{"--postgres-url", "amqp://a:b@:5672/"},
			connect: func(t *testing.T) func(url, driver string) (*sqlx.DB, error) {
				return func(url, driver string) (*sqlx.DB, error) {
					return nil, nil
				}
			},
			want:       true,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			p := &Postgres{
				logger: logger,
			}
			if tt.connect != nil {
				connectToPostgresFunc = tt.connect(t)
			}
			got, err := p.Configure(tt.args)
			if !tt.wantErr(t, err, fmt.Sprintf("Configure(%v)", tt.args)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Configure(%v)", tt.args)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestPostgres_IsSource(t *testing.T) {
	assert.Equalf(t, false, (&Postgres{}).IsSource(), "IsSource()")
}

func TestPostgres_IsSink(t *testing.T) {
	assert.Equalf(t, false, (&Postgres{}).IsSink(), "IsSink()")
}

func TestPostgres_IsStore(t *testing.T) {
	assert.Equalf(t, true, (&Postgres{}).IsStore(), "IsStore()")
}
