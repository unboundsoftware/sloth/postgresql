-- +goose Up
CREATE TABLE delayed
(
    id      SERIAL PRIMARY KEY,
    until   timestamptz not null,
    target  text        not null,
    payload bytea       not null
);
-- +goose Down
DROP TABLE delayed;
