/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"context"
	"embed"
	"encoding/json"
	"os"
	"sync"
	"time"

	"github.com/alecthomas/kong"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pressly/goose/v3"

	"gitlab.com/unboundsoftware/sloth/model"
	mp "gitlab.com/unboundsoftware/sloth/model/plugin"
)

type Config struct {
	DatabaseURL string `name:"postgres-url" env:"POSTGRES_URL" help:"URL to use to connect to Postgres"`
}

type Postgres struct {
	timeSource func() time.Time
	sleep      func(duration time.Duration)
	quit       chan bool
	loopDone   chan bool
	logger     hclog.Logger
	mutex      sync.Mutex
	started    bool
	client     *sqlx.DB
}

func (p *Postgres) Configure(args []string) (bool, error) {
	cli := &Config{}
	cmd, err := kong.New(
		cli,
		kong.Description("postgresql_store is a pluggable store for sloth"),
		kong.Writers(os.Stdout, os.Stdin),
	)
	if err != nil {
		return false, err
	}
	_, err = cmd.Parse(args)
	if err != nil {
		return false, err
	}

	if cli.DatabaseURL == "" {
		return false, nil
	}

	client, err := connectToPostgresFunc(cli.DatabaseURL, driverName)
	if err != nil {
		return false, err
	}
	p.client = client
	p.timeSource = time.Now
	p.sleep = time.Sleep
	p.quit = make(chan bool, 2)
	p.loopDone = make(chan bool, 2)

	return true, nil
}

func (p *Postgres) IsSource() bool {
	return false
}

func (p *Postgres) IsSink() bool {
	return false
}

func (p *Postgres) IsStore() bool {
	return true
}

func (p *Postgres) Start(handler model.Handler, quitter model.Quitter) error {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	go p.loop(handler.Handle, quitter)
	p.started = true
	return nil
}

func (p *Postgres) loop(handler func(model.Request) error, quitter model.Quitter) {
	defer func() {
		p.mutex.Lock()
		defer p.mutex.Unlock()
		p.started = false
		p.loopDone <- true
	}()
	for {
		select {
		case <-p.quit:
			return
		default:
			if err := p.process(handler); err != nil {
				quitter.Quit(err)
				return
			}
		}
	}
}

func (p *Postgres) process(handler func(request model.Request) error) error {
	now := p.timeSource()
	ctx := context.Background()

	rows, err := p.client.QueryContext(ctx, "SELECT id, target, payload FROM delayed WHERE until <= $1", now)
	if err != nil {
		p.logger.Error("error fetching requests", "error", err)
		return err
	}

	recordsFound := false
	for rows.Next() {
		recordsFound = true
		var id int
		var target string
		var payload []byte
		err := rows.Scan(&id, &target, &payload)
		if err != nil {
			p.logger.Error("error scanning data", "error", err)
			return err
		}

		request := &storedRequest{
			client:  p,
			ID:      id,
			Until:   now,
			Target:  target,
			Payload: payload,
		}
		if err := handler(request.Request()); err == nil {
			_ = request.Ack()
		} else {
			p.logger.Error("error handling request", "error", err)
			return err
		}
	}
	if !recordsFound {
		time.Sleep(time.Second)
	}
	return nil
}

func (p *Postgres) Stop() error {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	p.logger.Info("stopping")
	if p.started {
		p.quit <- true
	}
	return nil
}

func (p *Postgres) Add(request model.Request) error {
	data, err := request.Payload.MarshalJSON()
	if err != nil {
		return err
	}
	_, err = p.client.Exec("INSERT INTO delayed (until, target, payload) VALUES ($1,$2,$3)",
		request.DelayedUntil, request.Target, string(data))
	return err
}

var _ model.Store = &Postgres{}

type storedRequest struct {
	client  *Postgres
	ID      int
	Until   time.Time
	Target  string
	Payload json.RawMessage
}

func (s storedRequest) Request() model.Request {
	request := model.Request{
		DelayedUntil: s.Until,
		Target:       s.Target,
		Payload:      s.Payload,
	}
	return request
}

func (s storedRequest) Ack() error {
	return s.client.remove(s.ID)
}

func (p *Postgres) remove(id int) error {
	_, err := p.client.Exec("DELETE FROM delayed WHERE id=$1", id)
	return err
}

var connectToPostgresFunc = connectToPostgres

func connectToPostgres(dbUrl, driverName string) (*sqlx.DB, error) {
	db, err := sqlx.Open(driverName, dbUrl)
	if err != nil {
		return nil, err
	}

	if err := runMigrations(db, driverName); err != nil {
		return nil, err
	}

	return db, nil
}

func main() {
	logger := hclog.Default()
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: mp.Handshake,
		Plugins: map[string]plugin.Plugin{
			"store": &mp.StorePlugin{
				Impl: &Postgres{logger: logger},
			},
		},
		GRPCServer: plugin.DefaultGRPCServer,
	})
}

//go:embed migrations/*.sql
var embedMigrations embed.FS

func runMigrations(db *sqlx.DB, driverName string) error {
	goose.SetBaseFS(embedMigrations)

	return goose.Up(db.DB, "migrations")
}

const (
	driverName = "postgres"
)
