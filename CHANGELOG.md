# Changelog

All notable changes to this project will be documented in this file.

## [0.1.2] - 2024-10-22

### 🐛 Bug Fixes

- *(deps)* Update module github.com/hashicorp/go-plugin to v1.6.2
- *(deps)* Update module gitlab.com/unboundsoftware/sloth/model to v0.1.2

### ⚙️ Miscellaneous Tasks

- Use custom release notes
- Add CHANGES.md and VERSION to .gitignore

## [0.1.1] - 2024-10-17

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.9.0
- *(deps)* Update module github.com/alecthomas/kong to v0.9.0
- *(deps)* Update module github.com/pressly/goose/v3 to v3.19.1
- *(deps)* Update module github.com/pressly/goose/v3 to v3.19.2
- *(deps)* Update module github.com/hashicorp/go-hclog to v1.6.3
- *(deps)* Update module github.com/pressly/goose/v3 to v3.20.0
- *(deps)* Update module github.com/jmoiron/sqlx to v1.4.0
- *(deps)* Update module github.com/hashicorp/go-plugin to v1.6.1
- *(deps)* Update module github.com/pressly/goose/v3 to v3.21.0
- *(deps)* Update module github.com/pressly/goose/v3 to v3.21.1
- *(deps)* Update module github.com/pressly/goose/v3 to v3.22.0
- *(deps)* Update module github.com/alecthomas/kong to v1
- *(deps)* Update module github.com/alecthomas/kong to v1.2.0
- *(deps)* Update module github.com/alecthomas/kong to v1.2.1
- *(deps)* Update module github.com/pressly/goose/v3 to v3.22.1
- *(deps)* Update module gitlab.com/unboundsoftware/sloth/model to v0.1.1

### 💼 Other

- *(deps)* Bump github.com/alecthomas/kong from 0.8.0 to 0.8.1
- *(deps)* Bump github.com/stretchr/testify from 1.7.2 to 1.8.4
- *(deps)* Bump github.com/hashicorp/go-hclog from 0.14.1 to 1.5.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.15.1 to 3.16.0
- *(deps)* Bump github.com/hashicorp/go-plugin from 1.5.2 to 1.6.0
- *(deps)* Bump github.com/hashicorp/go-hclog from 1.5.0 to 1.6.1
- *(deps)* Bump github.com/DATA-DOG/go-sqlmock from 1.5.0 to 1.5.1
- *(deps)* Bump github.com/hashicorp/go-hclog from 1.6.1 to 1.6.2
- *(deps)* Bump github.com/pressly/goose/v3 from 3.16.0 to 3.17.0
- *(deps)* Bump github.com/DATA-DOG/go-sqlmock from 1.5.1 to 1.5.2
- *(deps)* Bump github.com/pressly/goose/v3 from 3.17.0 to 3.18.0

### ⚙️ Miscellaneous Tasks

- Update goreleaser image to v2.3.1
- Add Unbound Release in tag-only mode

### 🛡️ Security

- *(deps)* [security] bump golang.org/x/net from 0.7.0 to 0.17.0
- *(deps)* [security] bump google.golang.org/grpc

## [0.1.0] - 2023-10-12

### 🚀 Features

- Move to hashicorp/go-plugin

### 💼 Other

- *(deps)* Bump github.com/pressly/goose/v3 from 3.6.1 to 3.7.0
- *(deps)* Bump github.com/lib/pq from 1.10.6 to 1.10.7
- *(deps)* Bump github.com/pressly/goose/v3 from 3.7.0 to 3.8.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.8.0 to 3.9.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.9.0 to 3.10.0
- *(deps)* Bump github.com/lib/pq from 1.10.7 to 1.10.8
- *(deps)* Bump github.com/lib/pq from 1.10.8 to 1.10.9
- *(deps)* Bump github.com/pressly/goose/v3 from 3.10.0 to 3.11.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.11.0 to 3.11.2
- *(deps)* Bump github.com/pressly/goose/v3 from 3.11.2 to 3.13.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.13.0 to 3.13.1
- *(deps)* Bump github.com/pressly/goose/v3 from 3.13.1 to 3.13.4
- *(deps)* Bump github.com/pressly/goose/v3 from 3.13.4 to 3.14.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.14.0 to 3.15.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.15.0 to 3.15.1

### ⚙️ Miscellaneous Tasks

- Use Docker DinD version from variable
- Switch to manual rebases for Dependabot
- Update to Go 1.20
- Check goreleaser config and do a dry run

## [0.0.4] - 2022-07-20

### 💼 Other

- *(deps)* Bump gitlab.com/unboundsoftware/sloth/model

## [0.0.3] - 2022-06-24

### 🐛 Bug Fixes

- Use logger directly

### 💼 Other

- *(deps)* Bump gitlab.com/unboundsoftware/apex-mocks
- *(deps)* Bump gitlab.com/unboundsoftware/apex-mocks
- *(deps)* Bump github.com/pressly/goose/v3 from 3.5.3 to 3.6.0
- *(deps)* Bump github.com/pressly/goose/v3 from 3.6.0 to 3.6.1

## [0.0.2] - 2022-05-17

### 💼 Other

- Update to Go 1.18
- *(deps)* Bump github.com/lib/pq from 1.10.4 to 1.10.5
- *(deps)* Bump github.com/jmoiron/sqlx from 1.3.4 to 1.3.5
- *(deps)* Bump github.com/sanity-io/litter from 1.5.4 to 1.5.5
- *(deps)* Bump github.com/lib/pq from 1.10.5 to 1.10.6

<!-- generated by git-cliff -->
